# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
from PyQt5 import QtCore, QtWidgets, uic, QtGui
from PyQt5.QtCore import QTimer, QSize
from PyQt5.QtGui import QIcon
from ament_index_python.packages import get_package_share_directory

import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
class Publishers(Node):

    def __init__(self):
        super().__init__('teleop_publishers')
        self.pubsoftstop_ = self.create_publisher(Bool, 'softstop', 10)
        self.publight_ = self.create_publisher(Bool, 'light', 10)
        
    def publish_light(self, light_on = False):
        msg = Bool()
        msg.data = light_on
        self.publight_.publish(msg)

    def publish_stop(self):
        msg = Bool()
        msg.data = True
        self.pubsoftstop_.publish(msg)

class MainWindow(QtWidgets.QMainWindow):

    light_is_on = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ui_file = os.path.join(get_package_share_directory('spinrobot_teleop'), 'resource', 'teleop_gui.ui')
        stopsign = os.path.join(get_package_share_directory('spinrobot_teleop'), 'resource', 'Stopsign.svg')
        #uic.loadUi("resource/teleop_gui.ui", self)
        uic.loadUi(ui_file, self)
     	
        self.pushButton.clicked.connect(self.ToggleLight)
        self.stopButton.clicked.connect(self.StopSpinrobot)
        self.stopButton.setIcon(QIcon(stopsign))
        self.stopButton.setIconSize(QSize(150, 150))
        self.rospub_ = Publishers()
        
        self.timer = QTimer()
        self.timer.isSingleShot = False
        self.timer.timeout.connect(self.spin)
        self.timer.start(10) # [ms]
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose) 

    def ToggleLight(self):
        if self.light_is_on == True:
            self.light_is_on = False
        else:
            self.light_is_on = True
        self.rospub_.publish_light(self.light_is_on)

    def StopSpinrobot(self):
        self.rospub_.publish_stop()

    def spin(self):
        rclpy.spin_once(self.rospub_, timeout_sec=0.001)

    def closeEvent(self, event):
        self.timer.stop()
        self.rospub_.destroy_node()
        rclpy.shutdown()
        
def main(args=None):
    
    rclpy.init(args=args)
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater) 
    window = MainWindow()
    window.show()
    app.exec_()

if __name__ == '__main__':
    main()
