from setuptools import setup

package_name = 'spinrobot_calibrate'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name , 'resource/spin_calibration_window.ui']),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/resource', ['resource/spin_calibration_window.ui', 'resource/spinrobot_calibration.yaml'])
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Edward Hage',
    maintainer_email='edward@confirmat.nl',
    description='Calibration of wheel point of forward-backward speed and forward direction',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'wheel_calibration = spinrobot_calibrate.wheel_calibration:main'
        ],
    },
)
