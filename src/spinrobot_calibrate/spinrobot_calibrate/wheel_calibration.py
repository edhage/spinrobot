# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import os
import yaml
import pi_servo_hat
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import QTimer
from ament_index_python.packages import get_package_share_directory
class MainWindow(QtWidgets.QMainWindow):

    minvalue = 330 #pwm min 
    maxvalue = 430 #pwm max
    servostatus = False # live or not 
    calibrationfilename = 'spinrobot_calibration.yaml'
    validator = QtGui.QRegExpValidator(QtCore.QRegExp('^[0-9]{3}$'))
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ui_file = os.path.join(get_package_share_directory('spinrobot_calibrate'), 'resource', 'spin_calibration_window.ui')
        #uic.loadUi("resource/spin_calibration_window.ui", self)
        uic.loadUi(ui_file, self)
     	
        self.init_servo_values()
        self.init_pi_servo()
        self.pushButton.clicked.connect(self.GenerateFile)
        self.pushServoStatus.clicked.connect(self.ToggleServoStatus)
        
        self.timer = QTimer()
        self.timer.timeout.connect(self.Repeat)
        
        self.SliderConnect()
        self.comboBox.currentIndexChanged.connect(self.ComboChanged)
        
        self.SetSliders()   
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose) 
        
    def SliderConnect(self):
        self.LF_slider.setMinimum(self.minvalue)
        self.LF_slider.setMaximum(self.maxvalue)
        self.LF_slider.setSingleStep(1)
        self.LF_slider.valueChanged.connect(self.LFSlider)
        self.LC_slider.setMinimum(self.minvalue)
        self.LC_slider.setMaximum(self.maxvalue)
        self.LC_slider.setSingleStep(1)
        self.LC_slider.valueChanged.connect(self.LCSlider)
        self.LR_slider.setMinimum(self.minvalue)
        self.LR_slider.setMaximum(self.maxvalue)
        self.LR_slider.setSingleStep(1)
        self.LR_slider.valueChanged.connect(self.LRSlider)
        self.RF_slider.setMinimum(self.minvalue)
        self.RF_slider.setMaximum(self.maxvalue)
        self.RF_slider.setSingleStep(1)
        self.RF_slider.valueChanged.connect(self.RFSlider)
        self.RC_slider.setMinimum(self.minvalue)
        self.RC_slider.setMaximum(self.maxvalue)
        self.RC_slider.setSingleStep(1)
        self.RC_slider.valueChanged.connect(self.RCSlider)
        self.RR_slider.setMinimum(self.minvalue)
        self.RR_slider.setMaximum(self.maxvalue)
        self.RR_slider.setSingleStep(1)
        self.RR_slider.valueChanged.connect(self.RRSlider)

        self.LF_value.textChanged.connect(self.LFV)
        self.LC_value.textChanged.connect(self.LCV)
        self.LR_value.textChanged.connect(self.LRV)
        self.RF_value.textChanged.connect(self.RFV)
        self.RC_value.textChanged.connect(self.RCV)
        self.RR_value.textChanged.connect(self.RRV)

    def LFSlider(self,value):
        self.LF[self.comboBox.currentIndex()] = value
        self.LF_value.setText(str(value))
    
    def LCSlider(self, value):
        self.LC[self.comboBox.currentIndex()] = value
        self.LC_value.setText(str(value))
  
    def LRSlider(self,value):
        self.LR[self.comboBox.currentIndex()] = value
        self.LR_value.setText(str(value))
    
    def RFSlider(self, value):
        self.RF[self.comboBox.currentIndex()] = value
        self.RF_value.setText(str(value))

    def RCSlider(self,value):
        self.RC[self.comboBox.currentIndex()] = value
        self.RC_value.setText(str(value))
    
    def RRSlider(self, value):
        self.RR[self.comboBox.currentIndex()] = value
        self.RR_value.setText(str(value))
              
    def LFV(self, value):
        if len(value) == 0:
            return
        self.LF_slider.setValue(int(value))
    def LCV(self, value):
        if len(value) == 0:
            return
        self.LC_slider.setValue(int(value))
    def LRV(self, value):
        if len(value) == 0:
            return
        self.LR_slider.setValue(int(value))
    def RFV(self, value):
        if len(value) == 0:
            return
        self.RF_slider.setValue(int(value))
    def RCV(self, value):
        if len(value) == 0:
            return
        self.RC_slider.setValue(int(value))
    def RRV(self, value):
        if len(value) == 0:
            return
        self.RR_slider.setValue(int(value))
        
    def ComboChanged(self, value):
        # Set servos off
        self.servostatus = False
        self.timer.stop()
        self.SetServoStatusIndicator()
        self.SetSliders()        
        
    def SetSliders(self):
        value = self.comboBox.currentIndex()
        # Set sliders (plus attached values automatically)
        self.LFSlider(self.LF[value])
        self.LCSlider(self.LC[value])
        self.LRSlider(self.LR[value])
        self.RFSlider(self.RF[value])
        self.RCSlider(self.RC[value])
        self.RRSlider(self.RR[value])

    def init_servo_values(self):
        self.LF = [380, 380 , 380] # anglevalue, backward zero, forward zero
        self.LC = [380, 380 , 380]
        self.LR = [380, 380 , 380]
        self.RF = [380, 380 , 380]
        self.RC = [380, 380 , 380]
        self.RR = [380, 380 , 380]
        
        self.LF_value.setValidator(self.validator)
        self.LC_value.setValidator(self.validator)
        self.LR_value.setValidator(self.validator)
        self.RF_value.setValidator(self.validator)
        self.RC_value.setValidator(self.validator)
        self.RR_value.setValidator(self.validator)


    def set_all_angles(self):
        if (self.AllWithinRange() == True):
            self.Board.PCA9685.set_channel_word(6,0, self.LF[0])
            self.Board.PCA9685.set_channel_word(7,0, self.LC[0])
            self.Board.PCA9685.set_channel_word(8,0, self.LR[0])
            self.Board.PCA9685.set_channel_word(9,0, self.RF[0])
            self.Board.PCA9685.set_channel_word(10,0, self.RC[0])
            self.Board.PCA9685.set_channel_word(11,0, self.RR[0])
            print("angle LF: "  + str(self.LF[0]))
            print("angle LC: "  + str(self.LC[0]))
            print("angle LR: "  + str(self.LR[0]))
            print("angle RF: "  + str(self.RF[0]))
            print("angle RC: "  + str(self.RC[0]))
            print("angle RR: "  + str(self.RR[0]))
                        
    def AllWithinRange(self):
        for v in self.LF: 
            if self.WithinRange(v) == False:
                return False
        for v in self.LC: 
            if self.WithinRange(v) == False:
                return False
        for v in self.LR: 
            if self.WithinRange(v) == False:
                return False
        return True
     
    def WithinRange(self, value):
        if value >= self.minvalue:
            if value <= self.maxvalue:
                return True
        return False
        
    def set_all_motors(self, forward):
        if forward == True:
            i = 2
        else:
            i = 1
        self.Board.PCA9685.set_channel_word(0,0, self.LF[i])
        self.Board.PCA9685.set_channel_word(1,0, self.LC[i])
        self.Board.PCA9685.set_channel_word(2,0, self.LR[i])
        self.Board.PCA9685.set_channel_word(3,0, self.RF[i])
        self.Board.PCA9685.set_channel_word(4,0, self.RC[i])
        self.Board.PCA9685.set_channel_word(5,0, self.RR[i])
        print("motor LF: "  + str(self.LF[i]))
        print("motor LC: "  + str(self.LC[i]))
        print("motor LR: "  + str(self.LR[i]))
        print("motor RF: "  + str(self.RF[i]))
        print("motor RC: "  + str(self.RC[i]))
        print("motor RR: "  + str(self.RR[i]))

    def Repeat(self):
        if self.servostatus == True:
            if self.comboBox.currentIndex() == 0:
                self.set_all_angles()
            elif self.comboBox.currentIndex() == 1:
                self.set_all_motors(False)
            else:
                self.set_all_motors(True)

       
    def SetServoStatusIndicator(self):
        if self.servostatus == False:
            self.label_live.setText('<font color="gray">IDLE</font>')
        else:
            self.label_live.setText ( '<font color="red">LIVE</font>')

    def ToggleServoStatus(self):
        if self.servostatus == False:
            self.servostatus = True 
            self.timer.start(100)
            self.pushServoStatus.setText("Go Idle")
        else:
            self.servostatus = False
            self.timer.stop()
            self.pushServoStatus.setText("Go Live")
        self.SetServoStatusIndicator()
        
            
    def init_pi_servo(self):
        self.Board = pi_servo_hat.PiServoHat()
        self.Board.restart()
        self.Board.set_pwm_frequency(60)
        for i in range(12):
            self.Board.PCA9685.set_channel_word(i,1,0)
    		
    def GenerateFile(self):
        '''Button clicked. Generate file with all data'''
        i = 0
        angles = [self.LF[i], self.LC[i], self.LR[i], self.RF[i], self.RC[i],self.RR[i]]
        i = 1
        backward = [self.LF[i], self.LC[i], self.LR[i], self.RF[i], self.RC[i],self.RR[i]]
        i = 2
        forward = [self.LF[i], self.LC[i], self.LR[i], self.RF[i], self.RC[i],self.RR[i]]
        mydict = dict()
        mydict['/spinrobot_motordriver'] = dict()
        mydict['/spinrobot_motordriver']['ros__parameters'] = dict()
        mydict['/spinrobot_motordriver']['ros__parameters']['Forward Start'] = forward
        mydict['/spinrobot_motordriver']['ros__parameters']['Backward Start'] = backward
        mydict['/spinrobot_motordriver']['ros__parameters']['Forward Facing Position'] = angles
        mydict['/spinrobot_motordriver']['ros__parameters']['use_sim_time'] = False
        yaml_filename = os.path.join(get_package_share_directory('spinrobot_calibrate'), 'resource', self.calibrationfilename)
        with open(yaml_filename, 'w') as file:
            yaml.dump(mydict, file)
            print("Generated a file in: " + yaml_filename)
    	
def main(args=None):
    app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater) 
    window = MainWindow()
    window.show()
    app.exec_()

if __name__ == '__main__':
    main()


