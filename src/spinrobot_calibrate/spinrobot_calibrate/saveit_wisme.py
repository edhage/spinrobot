def saveit(self):
    i = 1
    print("motor LF: "  + str(self.LF[i]))
    print("motor LC: "  + str(self.LC[i]))
    print("motor LR: "  + str(self.LR[i]))
    print("motor RF: "  + str(self.RF[i]))
    print("motor RC: "  + str(self.RC[i]))
    print("motor RR: "  + str(self.RR[i]))
import yaml
mydict = dict()
mydict['/spinrobot_motordriver'] = dict()
mydict['/spinrobot_motordriver']['ros__parameters'] = dict()
mydict['/spinrobot_motordriver']['ros__parameters']['Forward Start'] = [371,371,368,390,389,386]
mydict['/spinrobot_motordriver']['ros__parameters']['Backward Start'] = [394,393,391,366,368,362]
mydict['/spinrobot_motordriver']['ros__parameters']['Forward Facing Position'] = [360, 391, 399, 400, 360, 377]
mydict['/spinrobot_motordriver']['ros__parameters']['use_sim_time'] = False
with open(r'wisme.yaml', 'w') as file:
    yaml.dump(mydict, file)
