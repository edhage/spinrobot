# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import pi_servo_hat
from rclpy.node import Node
from std_msgs.msg import Bool

class LightDriver(Node):
    
    lightchannel = 15

    def __init__(self):
        super().__init__('spinrobot_lightdriver')
        self.subscription = self.create_subscription( Bool, 'light', self.listener_callback, 10)
        try: 
            self.init_pi_servo() 
        except:
            self.get_logger().info('Could not connect to PiServoHat')

    def init_pi_servo(self):
        self.Board = pi_servo_hat.PiServoHat()
        self.Board.restart()
        self.Board.set_pwm_frequency(60)
        self.Board.PCA9685.set_channel_word(self.lightchannel,1,0)

    def listener_callback(self, msg):
        switchon = msg.data
        if switchon == True:
            self.get_logger().info('Got lightcommand: switch light ON')
            self.Board.PCA9685.set_channel_word(self.lightchannel,0, 2000)
        else:
            self.get_logger().info('Got lightcommand: switch light OFF')
            self.Board.PCA9685.set_channel_word(self.lightchannel,0, 200)

def main(args=None):
    rclpy.init(args=args)
    lightdriver = LightDriver()
    rclpy.spin(lightdriver)
    lightdriver.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
