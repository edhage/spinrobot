# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from std_msgs.msg import Float32MultiArray, MultiArrayDimension

class SpinController(Node):

    def __init__(self):
        super().__init__('spin_controller')
        self.subscription = self.create_subscription(Joy, 'joy', self.listener_callback, 10)
        self.publisher_ = self.create_publisher(Float32MultiArray, 'spinrobotcontrol', 10)

    def listener_callback(self, msg):
        #TODO: add scaling
        x = -1 * msg.axes[0] #8bitDO axis 0 = left = negative right = positive  (left joystick)
        y = msg.axes[1] #8bitDO axis 1 = front = positive back = negative  (left joystick)
        r = msg.axes[3] #8bitDO axis 3 = left = negative right = positive  (right joystick)

        #8bitDO axis 2 = left button not pushed = negative , pushed is les negative upto positive 
        #8bitDO axis 5 = right button not pushed = negative , pushed is les negative upto positive 
        # 2 or 5 held for safety (deadmans-knob)
		
        xmove = MultiArrayDimension()
        xmove.label = "x"
        xmove.size = 1
        xmove.stride = 1
        ymove = MultiArrayDimension()
        ymove.label = "y"
        ymove.size = 1
        ymove.stride = 1
        rmove = MultiArrayDimension()
        rmove.label = "r"
        rmove.size = 1
        rmove.stride = 1

        arg = Float32MultiArray()
        arg.layout.dim.append(xmove)
        arg.layout.dim.append(ymove)
        arg.layout.dim.append(rmove)

        arg.data = [x, y, r]

        self.publisher_.publish(arg)


def main(args=None):
    rclpy.init(args=args)
    spin_controller = SpinController()
    rclpy.spin(spin_controller)
    spin_controller.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
