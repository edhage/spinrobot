# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import numpy as np
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray, MultiArrayDimension

class DemoDrive(Node):

    def __init__(self):
        super().__init__('demo_drive')
        self.dt = 0.05
        self.timer_ = self.create_timer(self.dt, self.callback_timer)
        self.publisher_ = self.create_publisher(Float32MultiArray, 'spinrobotcontrol', 10)
        self.define_demo()
        self.runtime = 0.0
        self.softstop = False
        
    def define_demo(self):
        # move forward
        tprev = 3
        matrixstring = "[0,   0,  0,  0 ;\n1,   0,  0,  0 ;\n2, 0,  1,  0 ;\n3,   0,  0,  0 ;"
        
        #rotate left-back a complete circle in 10 seconds
        n = 40
        time = 10.0
        xc = -np.cos(np.arange(n)/n*2*np.pi)*0.6
        yc = -np.sin(np.arange(n)/n*2*np.pi)*0.6
        dt = time/n

        for i,xx in enumerate(xc):
            matrixstring = matrixstring + str(tprev+dt*(i+1)) + ", " + str(xx) + ", " + str(yc[i]) + ", 0;\n"
            
        matrixstring = matrixstring + str(tprev+dt*(i+1) + 0.1) + ", 0, 0, 0;\n"
        matrixstring = matrixstring + str(tprev+dt*(i+1) + 1.2)  + ", 0.35, -1.1, -0.08;\n"
        matrixstring = matrixstring + str(tprev+dt*(i+1) + 2.3)  + ", 0, 0, 0;\n"
        matrixstring = matrixstring + str(tprev+dt*(i+1) + 4.3)  + ", 0, 0, 0;\n"
        tprev = tprev+dt*(i+1) + 4.3

        # diamand linksom
        diam_dt = 1.5
        matrixstring += str(tprev+diam_dt*1)  + ", 0, 1, 0;\n"
        matrixstring += str(tprev+diam_dt*2)  + ", 0, 0, 0;\n"

        matrixstring += str(tprev+diam_dt*3)  + ", 1, -1, 0;\n"
        matrixstring += str(tprev+diam_dt*4)  + ", 0, 0, 0;\n"

        matrixstring += str(tprev+diam_dt*5)  + ", -1, -1, 0;\n"
        matrixstring += str(tprev+diam_dt*6)  + ", 0, 0, 0;\n"

        matrixstring += str(tprev+diam_dt*7)  + ", -1, 1, 0;\n"
        matrixstring += str(tprev+diam_dt*8)  + ", 0, 0, 0;\n"

        matrixstring += str(tprev+diam_dt*9)  + ", 1, 1, 0;\n"
        matrixstring += str(tprev+diam_dt*10)  + ", 0, 0, 0;\n"

        matrixstring += str(tprev+diam_dt*11)  + ", 0, -1, 0;\n"
        matrixstring += str(tprev+diam_dt*12)  + ", 0, 0, 0;\n"
        tprev = tprev+diam_dt*12
        
        # rotate
        matrixstring += str(tprev+1)  + ", 0, 0, 0;\n"
        matrixstring += str(tprev+1.2)  + ", 0, 0, 1;\n"
        matrixstring += str(tprev+4.55)  + ", 0, 0, 1;\n"
        matrixstring += str(tprev+4.75)  + ", 0, 0, 0;\n"

        matrixstring = matrixstring[:-2]+']'
        myvars = np.matrix(matrixstring)

        self.tlist = list()
        self.xlist = list()
        self.ylist = list()
        self.rlist = list()
        for myv in myvars:
            self.tlist.append(myv[0,0])
            self.xlist.append(myv[0,1])
            self.ylist.append(myv[0,2])
            self.rlist.append(myv[0,3])
                
    def get_interpolated(self, t, tlist, varlist):
        prev = 0
        current = 0
        counter = 0
        for i, tl in enumerate(tlist):
            prev = current
            current = tl
            if (current - t) >= 0 and (prev-t) <= 0 and current>prev:
                remainder = (t-prev)/(current-prev)
                answer = remainder*(varlist[i] - varlist[i-1]) + varlist[i-1]
                return answer
        answer = 0.0
        return answer

    def callback_timer(self):
        self.runtime = self.runtime + self.dt
        if self.runtime > self.tlist[-1] :
            self.runtime = 0
        xmove = MultiArrayDimension()
        xmove.label = "x"
        xmove.size = 1
        xmove.stride = 1
        ymove = MultiArrayDimension()
        ymove.label = "y"
        ymove.size = 1
        ymove.stride = 1
        rmove = MultiArrayDimension()
        rmove.label = "r"
        rmove.size = 1
        rmove.stride = 1

        arg = Float32MultiArray()
        arg.layout.dim.append(xmove)
        arg.layout.dim.append(ymove)
        arg.layout.dim.append(rmove)

        arg.data = [ 0.5* self.get_interpolated(self.runtime, self.tlist, self.xlist),
                        0.5 * self.get_interpolated(self.runtime, self.tlist, self.ylist),
                        0.5 * self.get_interpolated(self.runtime, self.tlist, self.rlist) ]
        if self.softstop == True:
            arg.data = [0,0,0]
        self.publisher_.publish(arg)


def main(args=None):
        rclpy.init(args=args)
        demo = DemoDrive()
        rclpy.spin(demo)
        demo.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()

