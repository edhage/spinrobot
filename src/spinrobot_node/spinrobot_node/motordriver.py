# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import pi_servo_hat
import numpy as np
from rclpy.node import Node

from std_msgs.msg import Float32MultiArray, Bool
from rcl_interfaces.msg import ParameterDescriptor as PD
class MotorDriver(Node):
    
    wheelpositions = list([[-222, 203], [-222, -51], [-222, -280], [222, 203], [222, -51], [222, -280]])
    RFactor = 1.0
    XFactor = 1.0 
    YFactor = 1.0    
    wheelspeed = [0.0, 0.0, 0.0 ,0.0, 0.0, 0.0] # list of 6 speeds (is updated)
    angles = [0.0, 0.0, 0.0 ,0.0, 0.0, 0.0] # list of 6 angles (is update)
    softstoptriggered = False

    def __init__(self):
        super().__init__('spinrobot_motordriver')
        self.subscription = self.create_subscription( Float32MultiArray, 'spinrobotcontrol', self.listener_callback, 10)
        softstopsub = self.create_subscription(Bool, 'softstop', self.trigger_softstop, 10)
        
        self.declare_parameter('Forward Start', [371,371,368,390,389,386],  PD(description="Motor power setting at the point that the motor starts to turn forward"))
        self.declare_parameter('Backward Start', [394,393,391,366,368,362],  PD(description="Motor power setting at the point that the motor starts to turn backward"))
        self.declare_parameter('Forward Facing Position', [360, 391, 399, 400, 360, 377],  PD(description="Rotation position at which each wheel faces forward"))

        self.forward_start = self.get_parameter('Forward Start').value
        self.backward_start = self.get_parameter('Backward Start').value
        self.centers_wheels = [int((x+y)/2) for x,y in zip(self.forward_start, self.backward_start)]
        self.centers_rotmotors = self.get_parameter('Forward Facing Position').value

        # After setting parameters
        try: 
            self.init_pi_servo() 
        except:
            self.get_logger().info('Could not connect to PiServoHat')
        
        self.init_calc()

    def trigger_softstop(self, msg):
        self.get_logger().info('Softstop trigger is received')
        self.softstoptriggered = msg.data
        self.set_speed(0, 0)
        self.set_speed(1, 0)
        self.set_speed(2, 0)
        self.set_speed(3, 0)
        self.set_speed(4, 0)
        self.set_speed(5, 0)

    def init_pi_servo(self):
        #analyse
        print( " NR   CENTER    ROT")
        print("======================")
        for i in range(6):
            print ( str(i) +"   " + str(self.centers_wheels[i]) + "   " + str(self.centers_rotmotors[i]) )

        self.Board = pi_servo_hat.PiServoHat()
        self.Board.restart()
        self.Board.set_pwm_frequency(60)

        for i in range(12):
            self.Board.PCA9685.set_channel_word(i,1,0)
        for i in range(6):
            self.Board.PCA9685.set_channel_word(i,0, self.centers_wheels[i])
        for i in range(6):
            self.Board.PCA9685.set_channel_word(i+6,0, self.centers_rotmotors[i])
   
    def init_calc(self):
        self.rellength = self.get_rel_length(self.wheelpositions)
        self.circlerad = self.get_circlerad(self.wheelpositions)
        
    def limit(self, myfloat):
        return max(min(myfloat,1),-1)

    def listener_callback(self, msg):
        x = msg.data[0]
        y = msg.data[1]
        r = msg.data[2]
        #self.get_logger().info('Got input x: %f y: %f Rotate: %f' %(x,y, r))

        self.calculate_motors(x,y,r)
        # x : -1 = left, 0 = rest , 1 = right
        # y: -1 = back, 0 = rest , 1 = front
        # r : -1 = turn left, 0 = rest , 1 = turn right
        self.drive_motors()
    
    def calculate_motors(self, xspeed, yspeed, rspeed):
        toggle = [ 1, 1, 1, 1, 1, 1 ]  # 1 or -1 for toggled

        for i, cr in enumerate(self.circlerad):
            #xsp = -np.cos(cr) * self.rellength[i] * rspeed * self.RFactor + xspeed * self.XFactor
            #ysp = -np.sin(cr) * self.rellength[i] * rspeed * self.RFactor + yspeed * self.YFactor
            xsp = -np.cos(cr) * self.rellength[i] * rspeed * self.RFactor + xspeed * self.XFactor
            ysp = -np.sin(cr) * self.rellength[i] * rspeed * self.RFactor + yspeed * self.YFactor

            self.wheelspeed[i] = np.sqrt( xsp*xsp + ysp*ysp )
            newalpha = np.arctan2(-xsp, ysp) * 180.0/3.14159265
            if newalpha < -90.0:
                self.angles[i] = newalpha + 180.0
                toggle[i] = -1
            elif newalpha > 90.0:
                self.angles[i] = newalpha - 180.0
                toggle[i] = -1
            else:
                self.angles[i] = newalpha
        self.wheelspeed[0] = self.wheelspeed[0] * toggle[0] #* -1
        self.wheelspeed[1] = self.wheelspeed[1] * toggle[1] #* -1
        self.wheelspeed[2] = self.wheelspeed[2] * toggle[2] #* -1
        self.wheelspeed[3] = self.wheelspeed[3] * toggle[3]
        self.wheelspeed[4] = self.wheelspeed[4] * toggle[4]
        self.wheelspeed[5] = self.wheelspeed[5] * toggle[5]

    def get_rel_length(self, wheelpositions):
        length = list()
        for wpos in wheelpositions:
            length.append(np.sqrt(wpos[0] * wpos[0] + wpos[1] * wpos[1]))
        maxlength = max(length)
        for i, le in enumerate(length):
            length[i] = length[i] / maxlength
        return length

    def get_circlerad(self, wheelpositions):
        circlerad = list()
        for wpos in wheelpositions:
            circlerad.append(np.arctan2(-wpos[0], wpos[1]))
        return circlerad
    
    def drive_motors(self):
        if (self.softstoptriggered == False):
            self.set_speed(0, self.wheelspeed[0]) # WLF
            self.set_speed(1, self.wheelspeed[1]) # WLC
            self.set_speed(2, self.wheelspeed[2]) # WLR
            self.set_speed(3, self.wheelspeed[3]) # WRF
            self.set_speed(4, self.wheelspeed[4]) # WRC
            self.set_speed(5, self.wheelspeed[5]) # WRR
        
            self.set_angle(0, self.angles[0])  # SLF
            self.set_angle(1, self.angles[1])  # SLC
            self.set_angle(2, self.angles[2])  # SLR
            self.set_angle(3, self.angles[3])  # SRF
            self.set_angle(4, self.angles[4])  # SRC
            self.set_angle(5, self.angles[5])  # SRR
        else:
            self.set_speed(0, 0)
            self.set_speed(1, 0)
            self.set_speed(2, 0)
            self.set_speed(3, 0)
            self.set_speed(4, 0)
            self.set_speed(5, 0)


    def set_angle(self, rotmotor, angle):
        ''' angle from -105 deg to 105 deg (for spinrobot -90 to 90)
        rotmotor = 0.. 5 '''
        totstroke = 508
        totangle = 210
        angle = max(min(angle, totangle/2), -totangle/2) # stroke limit
        pwm = int(round(self.centers_rotmotors[rotmotor] + (totstroke/totangle * angle)) )
        #self.get_logger().info(' angle of rotmotor %d value: %d' %(rotmotor,pwm)) #analyse
        try:
            self.Board.PCA9685.set_channel_word(6+rotmotor,0,pwm)
        except OSError as err:
            self.get_logger().info('OSError: cannot set angle of rotmotor %d value: %d, message: %s' %(rotmotor,pwm,str(err)))
        return pwm
    
    def set_speed(self, motor, speed):
        ''' speed from -1 to 1  (actually 0.05 to 0.7 and -0.7 to -0.05)
        motor = 0..5'''
        totstroke = 508
        speed = max(min(speed, 1), -1) # speed limit
        
        if self.forward_start[motor] > self.backward_start[motor]:
            direction = 1
        else:
            direction = -1
        
        if speed > 0:
            startpunt = self.forward_start[motor]
        elif speed < 0:
            startpunt = self.backward_start[motor]
        else:
            startpunt = 380
            
        pwm = int(round(startpunt + direction*totstroke/2 * 0.7 * speed))

        try:
            self.Board.PCA9685.set_channel_word(motor,0,pwm)
        except OSError as err:
            self.get_logger().info('OSError: cannot set speed of motor %d value: %d, message: %s' %(motor,pwm,str(err)))

        return pwm
    
    def speed_fric_compensate(self, motor,speed):
        ''' compensate fot hardware deadband'''
        print("motor="+str(motor))
        if self.forward_start[motor] > self.backward_start[motor]:
            direction = 1
        else:
            direction = -1
        if speed > 0:
            return (self.forward_start[motor], direction)
        elif speed <0:
            return (self.backward_start[motor], direction)


def main(args=None):
    rclpy.init(args=args)
    motordriver = MotorDriver()
    rclpy.spin(motordriver)
    motordriver.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
