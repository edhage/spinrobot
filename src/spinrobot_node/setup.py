from setuptools import setup

package_name = 'spinrobot_node'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Edward Hage',
    maintainer_email='edward@confirmat.nl',
    description='Spinrobot nodes',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'spincontroller = spinrobot_node.spincontroller:main',
            'spincontroller_twist = spinrobot_node.spincontroller_twist:main',
            'motordriver = spinrobot_node.motordriver:main',
            'lightdriver = spinrobot_node.lightdriver:main'
        ],
    },
)
