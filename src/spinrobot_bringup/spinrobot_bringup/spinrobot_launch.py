import os
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    try:
        CALIBRATION_FILE = os.path.join(get_package_share_directory('spinrobot_calibrate'), 'resource', 'spinrobot_calibration.yaml')
    except:
        return False

    return LaunchDescription([
        Node(
            package='joy_linux',
            executable='joy_linux_node',
            output='screen',
            emulate_tty=True,
            ), 
        Node(
            package='spinrobot_node',
            executable='lightdriver',
            output='screen',
            emulate_tty=True,
        ), 
        Node(
            package='spinrobot_node',
            executable='spincontroller',
            output='screen',
            emulate_tty=True,
        ), 
        Node(
            package='spinrobot_node',
            executable='motordriver',
            output='screen',
            emulate_tty=True,
            parameters=[ CALIBRATION_FILE ]
        ),
    ])
