# Copyright 2021-2022 Edward Hage
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    CALIBRATION_FILE = os.path.join(get_package_share_directory('spinrobot_calibrate'), 'resource', 'spinrobot_calibration.yaml')

    return LaunchDescription([
        #Node(
        #    package='teleop_twist_keyboard',
        #    executable='teleop_twist_keyboard',
        #    output='screen',
        #    emulate_tty=True,
        #    ), 
        Node(
            package='spinrobot_node',
            executable='spincontroller_twist',
            output='screen',
            emulate_tty=True,
        ), 
        Node(
            package='spinrobot_node',
            executable='lightdriver',
            output='screen',
            emulate_tty=True,
        ), 
        Node(
            package='spinrobot_node',
            executable='motordriver',
            output='screen',
            emulate_tty=True,
            parameters=[ CALIBRATION_FILE ]
        ),
    ])
