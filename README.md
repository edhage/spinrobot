## Spinrobot

This repository consists of ROS2 programs to be able to control the spinrobot. [Here the build instructions](https://www.instructables.com/Spin-Robot/) can be found. And the 3D printable parts can be [downloaded from here](https://cults3d.com/en/3d-model/gadget/spin-robot).
The required hardware is a Raspberry Pi (RPi). It has been tested succesfully with a RPi 3B+, and a RPi 4.

---

## Prerequisites: ROS on a Pi and X11 forwarding

### Pi SW installation

You will need to install ROS2 on Ubuntu on the RPi. You will need a 32MB SD-card.
Here is what to do. [I used this webpage as a source](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview)

#### Burn SD card with Ubuntu
The easiest way to burn the SD card and dowload the Raspberry Pi imager and choose “Other general purpospe OS”  and then “Ubuntu server 20.04.4 LTS”.

Put the SD card in the RPi and bootup (I have monitor and mouse installed). Login on screen with “ubuntu” as paasword and login ID.
You need to set a new password (for reference of you can use password <b>spinrover</b>.

If you have no monitor and keyboard make sure you have ethernet (or wifi) installed as per the above mentioned document, and log in remotely (Putty is recommended).

#### Install other packages
Perform the following commands in a terminal:

```
sudo apt update
sudo apt upgrade
sudo apt install ghostscript
```

Than install RO2 (here explained installation using Debian package), [following this document](https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html)

All ROS2 prerequisites will be installed by default so continue with:

```
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
sudo apt update
```

Do not install the full package but the base package like this:

```
sudo apt install ros-galactic-ros-base
```

Than add a line to .bashrc with:

```
cd ~
echo "source /opt/ros/galactic/setup.bash" >> .bashrc
```

Then install the following:

```
sudo apt install python3-colcon-common-extensions
sudo apt install pip
sudo pip install sparkfun-pi-servo-hat
sudo groupadd --system i2c
sudo usermod ubuntu -aG i2c
sudo chgrp i2c /dev/i2c-1
sudo chmod 660 /dev/i2c-1
sudo apt-get install i2c-tools
```

Then install a light desktop that will be used for X11 forwarding:

```
sudo apt update
sudo apt upgrade
sudo apt install lubuntu-desktop
```

During  installation you will be prompted for a choice between gdm3 and sddm , I chose <b>sddm</b>. (But not sure if it will not work with gdm3).

If you did not install wifi before than do it now with:

```
sudo apt install network-manager
sudo apt install wireless-tools
sudo nmcli --ask dev wifi connect <network-ssid>
```
Remark: <network-ssid> is your own network ssid, you can fill it in now.

--- 

The password of your network will be asked and you have to fill it in. It will be stored on the computer so you have to do it only once.
[This page helped me get wifi working](https://www.makeuseof.com/connect-to-wifi-with-nmcli/)

---

## X11 Forwarding
I made some GUIS for the robot (wheelcalibration and on-screen-emergency stop) and they can be used remotely on Windows or Linux via X11 forwarding. Here will be described to accomplish X11 forwarding on a Windows (10) computer.

You will need to install two programs. 
[download and install Putty ](https://www.putty.org/)
[download and install VcXsrv Windows X server](https://sourceforge.net/projects/vcxsrv/)

Via Windows start you can find VcXsrv as the application Xlaunch. You must launch it, before you start and remote GUI.
You need Putty to contact the Pi (via IP4 address) and need to switch on X11 Forwarding on Putty.

On the Pi X11 forwarding needs to be enabled, this is done by editing the file <b>/etc/ssh/ssh_config</b> as sudo.
You need to out ForwardX11 to 'yes' and ForwardX11Trusted to 'yes'.
[This webpage helped me](https://www.answertopia.com/ubuntu/displaying-ubuntu-applications-remotely-x11-forwarding/)

---

### Clone the repository
Basically all preparations are done. You must now clone this repository and build it. Since the programs are in Python nothing really is built, but the programs are put in the correct directory and ros2 can now run or launch the programs in the correct ROS-like manner.

```
cd ~
mkdir ros_ws
cd ros_ws
git clone https://edhage@bitbucket.org/edhage/spinrobot.git
colcon build
source install/setup.bash
```





---
